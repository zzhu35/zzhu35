/*
** server.c -- a stream socket server demo
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>

#include "debug.h"

//#define PORT "8080"  // the port users will be connecting to

#define BACKLOG 10	 // how many pending connections queue will hold

#define MAXDATASIZE 1048576 // max number of bytes we can get at once 1MB
#define MAX_FILE_NAME 512

void sigchld_handler(int s)
{
	while(waitpid(-1, NULL, WNOHANG) > 0);
}

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int main(int argc, char** argv)
{
	if (argc != 2){
		printf("Usage: ./server port");
	}
	
	int sockfd, new_fd;  // listen on sock_fd, new connection on new_fd
	struct addrinfo hints, *servinfo, *p;
	struct sockaddr_storage their_addr; // connector's address information
	socklen_t sin_size;
	struct sigaction sa;
	int yes=1;
	char s[INET6_ADDRSTRLEN];
	int rv;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE; // use my IP

	if ((rv = getaddrinfo(NULL, argv[1], &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	// loop through all the results and bind to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
				p->ai_protocol)) == -1) {
			perror("server: socket");
			continue;
		}

		if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
				sizeof(int)) == -1) {
			perror("setsockopt");
			exit(1);
		}

		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("server: bind");
			continue;
		}

		break;
	}

	if (p == NULL)  {
		fprintf(stderr, "server: failed to bind\n");
		return 2;
	}

	freeaddrinfo(servinfo); // all done with this structure

	if (listen(sockfd, BACKLOG) == -1) {
		perror("listen");
		exit(1);
	}

	sa.sa_handler = sigchld_handler; // reap all dead processes
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	if (sigaction(SIGCHLD, &sa, NULL) == -1) {
		perror("sigaction");
		exit(1);
	}

	printf("server: waiting for connections...\n");

	while(1) {  // main accept() loop
		sin_size = sizeof their_addr;
		new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);
		if (new_fd == -1) {
			perror("accept");
			continue;
		}

		inet_ntop(their_addr.ss_family,
			get_in_addr((struct sockaddr *)&their_addr),
			s, sizeof s);
		printf("server: got connection from %s\n", s);

		if (!fork()) { // this is the child process
			close(sockfd); // child doesn't need the listener

			char in_argument[MAX_FILE_NAME+1];
			in_argument[0] = '\0';
			in_argument[MAX_FILE_NAME] = '\0';

			// get http request from socket
			int in_bytes = recv(new_fd, in_argument, MAX_FILE_NAME, 0);
			if (in_bytes == MAX_FILE_NAME) rprintf("server: input file name might exceed limit\n");

			//parse request
			char http_h[8];
			char in_file_name[MAX_FILE_NAME];
			char http_v[16];

			http_h[0] = '\0';
			http_v[0] = '\0';
			in_file_name[0] = '\0';
			char* filename = in_file_name+1; // remove beginning slash

			sscanf(in_argument, "%s%s%s", http_h, in_file_name, http_v);

			// check if request is GET
			if (strncmp(http_h, "GET", 2)){
				// not GET, send 400
				if (send(new_fd, "HTTP/1.1 400 Bad Request\n\n", 26, 0) == -1)
					perror("send");
				close(new_fd);
				exit(0);
			}
			// attempt to open file
			printf("Preparing content %s\n", filename);
			FILE* fp = fopen(filename, "rb");
			if (!fp){
				// file not readable, send 404
				if (send(new_fd, "HTTP/1.1 404  Not Found\n\n", 25, 0) == -1)
				perror("send");
				close(new_fd);
				exit(0);
			}

			// send HTTP header
			char http_r[64];
			fseek(fp, 0, SEEK_END);
			int file_length = ftell(fp);
			rewind(fp);

			sprintf(http_r, "HTTP/1.1 200 OK\nContent-Length:%d\n\n", file_length);
			if (send(new_fd, http_r, strlen(http_r), 0) == -1)
				perror("send");

			//sleep(2);
			//========================================================// start transfering file
			void* buf = malloc(MAXDATASIZE);
			size_t bytes_read, total_sent = 0;
			while(bytes_read = fread(buf, 1, MAXDATASIZE, fp)){
				size_t bytes_sent = 0;
				// keep sending until whole buffer airborne
				while (bytes_sent != bytes_read){
					int bytes_sent_this_time = 0;
					if ((bytes_sent_this_time = send(new_fd, buf+bytes_sent, bytes_read-bytes_sent, 0)) == -1)
						perror("send");
					bytes_sent += bytes_sent_this_time;
				}
				total_sent += bytes_sent;
			}
			printf("Sent %lu bytes to client.\nDone.\n", total_sent);
			//====================================================// end transfering file

			close(new_fd);
			fclose(fp);
			free(buf);
			exit(0);
		}
		close(new_fd);  // parent doesn't need this
	}

	return 0;
}

