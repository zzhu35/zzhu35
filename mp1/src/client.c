/*
** client.c -- a stream socket client demo
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <arpa/inet.h>


#include "debug.h"


// #define PORT "80" // the port client will be connecting to 

#define MAXDATASIZE 1048576 // max number of bytes we can get at once 1MB
#define MAX_HOSTNAME 128
#define MAX_TARGET_NAME 512
#define MAX_PORT 7
#define SEND_BUF_SIZE 512

#define OUTFILE "output"

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int main(int argc, char *argv[])
{
	int sockfd, numbytes;  

	struct addrinfo hints, *servinfo, *p;
	int rv;
	char s[INET6_ADDRSTRLEN];

	if (argc != 2) {
	    fprintf(stderr,"usage: client hostname\n");
	    exit(1);
	}

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;


	char hostname[MAX_HOSTNAME];
	char target_raw[MAX_TARGET_NAME];
	char port_raw[MAX_PORT];
	hostname[0] = '\0';
	target_raw[1] = '\0';
	sprintf(port_raw, " 80"); // default HTTP port

	char* start_scan = argv[1];
	char* target = target_raw+1;
	char* port = port_raw+1;

	// determine input pattern
	if (strstr(argv[1], "http://") || strstr(argv[1], "HTTP://")){
		start_scan += 7;
	}else if (strstr(argv[1], "https://") || strstr(argv[1], "HTTPS://")){
		start_scan += 8;
	}
	if (strstr(start_scan, ":")){
		sscanf(start_scan, "%[^:]%[^/]%s", hostname, port_raw, target_raw);
	}else{
		sscanf(start_scan, "%[^/]%s", hostname, target_raw);
	}


	gprintf("hostname:%s\nport:%s\ntarget:%s\n", hostname, port, target);



	if ((rv = getaddrinfo(hostname, port, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	// loop through all the results and connect to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
				p->ai_protocol)) == -1) {
			perror("client: socket");
			continue;
		}

		if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("client: connect");
			continue;
		}

		break;
	}

	if (p == NULL) {
		fprintf(stderr, "client: failed to connect\n");
		return 2;
	}

	inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),
			s, sizeof s);
	printf("client: connecting to %s\n", s);

	freeaddrinfo(servinfo); // all done with this structure

	char msg_to_send[SEND_BUF_SIZE];
	sprintf(msg_to_send, "GET /%s HTTP/1.1\n\n", target);
	
	int bytes_sent = send(sockfd, msg_to_send, strlen(msg_to_send), 0);
	printf("Sent %d bytes to %s\n", bytes_sent, s);


	FILE* fp = fopen(OUTFILE, "wb");
	if (!fp) {
		fprintf(stderr, "Cannot open output file %s\n", OUTFILE);
		close(sockfd);
		return 0;
	}

	size_t total_written = 0;
	int expected = 0, resp = 0;
	memset(msg_to_send, 0, SEND_BUF_SIZE);
	int first_rcv = 0;
	if ((first_rcv = recv(sockfd, msg_to_send, SEND_BUF_SIZE, 0)) == -1) { // Well, just use previous send buffer to hold HTTP header...
		perror("recv");
		exit(1);
	}
	// @TODO should use PEEK to intercept HTTP header. Otherwise there are race conditions. Current solution, server sleep() after sending header.
	char http_v[20];
	http_v[0] = '\0';
	sscanf(msg_to_send, "%s%d", http_v, &resp);

	// get resp code
	if (resp != 200){
		printf("Server not OK. HTTP response code %d\n", resp);
		fclose(fp);
		close(sockfd);
		return 0;
	}
	printf("Server returned response code 200.\n");

	// get file size
	char* pos = strstr(msg_to_send, "Content-Length:");

	int has_length = 1;
	if (!pos){
		printf("No file length specified.\n");
		// @TODO has bug when no length is specified. So client does not work with some internet
		has_length = 0;
		fclose(fp);
		close(sockfd);
		return 0;
	}

	char skip; // skip ':'
	sscanf(pos, "%[^:]%c%d", http_v, &skip, &expected); // Well, just use http_v again...
	printf("Expecting %d bytes from server.\n", expected);


	//==========================================================// start receiving file
	//chdeck if buffer already have some data
	total_written = 0;
	pos = strstr(msg_to_send, "\n\n");
	if (pos){
		pos += 2;
		total_written += fwrite(pos, 1, first_rcv-(pos-msg_to_send), fp);
	}

	char* buf = (char*)malloc(MAXDATASIZE);
	if (!buf){
		printf("malloc failed!\n");
		fclose(fp);
		close(sockfd);
		return -1;
	}
	printf("00%%");
	int stream_closed = 0;
	printf("%d\n",total_written);
	while (total_written != expected){
		stream_closed = 1;
		int rcved_this_time = 0;
		if ((rcved_this_time = recv(sockfd, buf, MAXDATASIZE, 0)) == -1) {
			perror("recv");
			exit(1);
		}
		total_written += fwrite(buf, 1, rcved_this_time, fp);
		//printf("%d\n", rcved_this_time);
		printf("\b\b\b%2d%%", 100*total_written/expected);
	}
	printf("\nWritten %lu bytes to file %s\nDone.\n", total_written, OUTFILE);
	//===================================================================================// end receiving file
	
	fclose(fp);
	close(sockfd);
	free(buf);
	return 0;
}

