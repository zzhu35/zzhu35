#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include <iostream>
#include <algorithm>

using namespace std;

static FILE *fpOut, *topo, *msg, *change;


struct Gnode{
    vector<int> neighbors;
    vector<int> linkcosts;
};

struct Tabentry{
    int cost;
    int next;
    void print(){
        cout << " next: " << next << " cost: " << cost << endl;
    }
};

void vec2str(vector<int> in){
    for (int n : in){
        cout << n << " ";
    }
}
void map2str(map<int, int> in){
    for (auto it = in.begin(); it != in.end(); it++){
        cout << it->first << " " << it->second << endl;
    }
    cout << endl;
}

int minmap(map<int, int>& in, map<int, bool>& visited){
    auto min = in.begin();
    while(min != in.end()){
        if (visited[min->first]) min++;
        else if (min->second == -999) min++;
        else break;
    }
    if (min == in.end()) return 0;
    auto it = min;
    it++;
    for (; it != in.end(); it++){
        if (it->second == -999) continue;
        if (visited[it->first]) continue;
        if (it->second < min->second) min = it;
        else if (it->second == min->second && it->first < min->first) min = it;
    }
    return min->first;
}

map<int, Tabentry> compute(map<int, Gnode>& graph, int src){
    map<int, int> worksheet;
    map<int, bool> visited;
    map<int, int> nexthop;
    map<int, int> lastnode;
    for (auto it = graph.begin(); it != graph.end(); it++){
        worksheet[it->first] = -999;
        visited[it->first] = false;
        nexthop[it->first] = src;
        lastnode[it->first] = 999;
    }
    Gnode& srcnode = graph[src];
    for (int i = 0; i < srcnode.neighbors.size(); i++){
        worksheet[srcnode.neighbors[i]] = srcnode.linkcosts[i];
        nexthop[srcnode.neighbors[i]] = srcnode.neighbors[i];
        lastnode[srcnode.neighbors[i]] = src;
    }
    worksheet[src] = 0;
    visited[src] = true;
    
    int next = -1;
    while ((next = minmap(worksheet, visited))){
        Gnode& neighbor = graph[next];
        visited[next] = true;
        for (int neinei = 0; neinei < neighbor.neighbors.size(); neinei++){
            if (neighbor.linkcosts[neinei] < 0) continue;
            if (worksheet[neighbor.neighbors[neinei]] == -999){
                worksheet[neighbor.neighbors[neinei]] = worksheet[next] + neighbor.linkcosts[neinei];
                int& hop = nexthop[neighbor.neighbors[neinei]];
                hop = next;
                while(hop != nexthop[hop]) hop = nexthop[hop];
                lastnode[neighbor.neighbors[neinei]] = next;
            }
            else if (worksheet[neighbor.neighbors[neinei]] > worksheet[next] + neighbor.linkcosts[neinei]){
                worksheet[neighbor.neighbors[neinei]] = worksheet[next] + neighbor.linkcosts[neinei];
                int& hop = nexthop[neighbor.neighbors[neinei]];
                hop = next;
                while(hop != nexthop[hop]) hop = nexthop[hop];
                lastnode[neighbor.neighbors[neinei]] = next;
            }
            // else if (worksheet[neighbor.neighbors[neinei]] == worksheet[next] + neighbor.linkcosts[neinei] && nexthop[neighbor.neighbors[neinei]] < nexthop[next]){
            //     worksheet[neighbor.neighbors[neinei]] = worksheet[next] + neighbor.linkcosts[neinei];
            //     int& hop = nexthop[neighbor.neighbors[neinei]];
            //     hop = next;
            //     while(hop != nexthop[hop]) hop = nexthop[hop];
            //     lastnode[neighbor.neighbors[neinei]] = next;
            // }
            else if (worksheet[neighbor.neighbors[neinei]] == worksheet[next] + neighbor.linkcosts[neinei] && lastnode[neighbor.neighbors[neinei]] > next){
                worksheet[neighbor.neighbors[neinei]] = worksheet[next] + neighbor.linkcosts[neinei];
                int& hop = nexthop[neighbor.neighbors[neinei]];
                hop = next;
                while(hop != nexthop[hop]) hop = nexthop[hop];
                lastnode[neighbor.neighbors[neinei]] = next;
            }
        }
    }

    map<int, Tabentry> ret;
    for (auto it = worksheet.begin(); it != worksheet.end(); it++){
        ret[it->first] = Tabentry();
        ret[it->first].next = nexthop[it->first];
        ret[it->first].cost = it->second;
    }
    return ret;
}


map<int, map<int, Tabentry>> createtable(map<int, Gnode>& graph){
    map<int, map<int, Tabentry>> ret;
    for (auto it = graph.begin(); it != graph.end(); it++){
        ret[it->first] = compute(graph, it->first);
    }
    return ret;
}

void writetable(map<int, map<int, Tabentry>>& routetables){
    for (auto it = routetables.begin(); it != routetables.end(); it++){
        map<int, Tabentry>& singletable = it->second;
        for (auto itit = singletable.begin(); itit != singletable.end(); itit++){
            if (itit->second.cost < 0) continue;
            fprintf(fpOut, "%d %d %d\n", itit->first, itit->second.next, itit->second.cost);
        }
    }
}

void sendmessages(map<int, map<int, Tabentry>>& routetables, char* file){
    msg = fopen(file, "r");

    char s[256];
    memset(s, '\0', 256);
    while (fgets(s, 256, msg)){
        char* message = NULL;
        int count = 0;
        int src, dst;
        for(int i = 0; i < strlen(s); i++){
            if (s[i] == ' ') count++;
            if (count == 2){
                s[i] = '\0';
                message = &s[i+1];
            }
        }
        sscanf(s, "%d %d", &src, &dst);
        if (routetables[src][dst].cost < 0){
            fprintf(fpOut, "from %d to %d cost infinite hops unreachable message %s", src, dst, message);
            fclose(msg);
            return;
        }
        fprintf(fpOut, "from %d to %d cost %d hops ", src, dst, routetables[src][dst].cost);
        int curr = src;
        bool enter = false;
        while (curr != dst){
            enter = true;
            fprintf(fpOut, "%d ", curr);
            curr = routetables[curr][dst].next;
        }
        if (!enter) fprintf(fpOut, " ");
        fprintf(fpOut, "message %s", message);

        memset(s, '\0', 256);
    }
    fclose(msg);
}

int main(int argc, char** argv) {
    //printf("Number of arguments: %d", argc);
    if (argc != 4) {
        printf("Usage: ./linkstate topofile messagefile changesfile\n");
        return -1;
    }

    fpOut = fopen("output.txt", "w");

    map<int, Gnode> graph;
    
    char s[256];
    memset(s, '\0', 256);

    topo = fopen(argv[1], "r");
    while (fgets(s, 256, topo)){
        int start, end, cost;
        sscanf(s, "%d %d %d", &start, &end, &cost);
        if (graph.find(start) == graph.end()){
            graph[start] = Gnode();
        }
        Gnode& startnode = graph[start];
        startnode.neighbors.push_back(end);
        startnode.linkcosts.push_back(cost);

        if (graph.find(end) == graph.end()){
            graph[end] = Gnode();
        }
        Gnode& endnode = graph[end];
        endnode.neighbors.push_back(start);
        endnode.linkcosts.push_back(cost);
        memset(s, '\0', 256);
    }
    fclose(topo);

    map<int, map<int, Tabentry>> routetables = createtable(graph);

    writetable(routetables);
    sendmessages(routetables, argv[2]);

    change = fopen(argv[3], "r");
    memset(s, '\0', 256);
    while (fgets(s, 256, change)){
        int start, end, cost;
        sscanf(s, "%d %d %d", &start, &end, &cost);
        if (graph.find(start) == graph.end()){
            graph[start] = Gnode();
        }
        if (graph.find(end) == graph.end()){
            graph[end] = Gnode();
        }
        Gnode& startnode = graph[start];
        Gnode& endnode = graph[end];
        int i;
        bool found = false;
        for (i = 0; i < startnode.neighbors.size(); i++){
            if (startnode.neighbors[i] == end){
                startnode.linkcosts[i] = cost;
                found = true;
                break;
            }
        }
        for (i = 0; i < endnode.neighbors.size(); i++){
            if (endnode.neighbors[i] == start){
                endnode.linkcosts[i] = cost;
                found = true;
                break;
            }
        }
        if (!found){
            startnode.neighbors.push_back(end);
            startnode.linkcosts.push_back(cost);
            endnode.neighbors.push_back(start);
            endnode.linkcosts.push_back(cost);
        }

        memset(s, '\0', 256);
        routetables = createtable(graph);
        writetable(routetables);
        sendmessages(routetables, argv[2]);
    }




    fclose(fpOut);
    fclose(change);

    return 0;
}

