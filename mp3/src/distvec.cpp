#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <map>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
static FILE *fpOut, *msg;
struct r_node{
	map<int, int> neighbor;
	map<int, int> path;
	map<int, int> dest_table; 
};




map<int, r_node*> cost_table(map<int, r_node*> &temp){

	
	for(auto i = temp.begin(); i != temp.end(); i++){
		r_node * current = i->second;

		map<int, int> neighbors = current->neighbor;
		map<int, int> curr_dest = current->dest_table;

		for(auto j = neighbors.begin(); j != neighbors.end(); j++){
			int nei_index = j->first;
			int dist = neighbors[nei_index];
			if (dist == -999) {
				continue;
			}
			r_node * nei = temp[nei_index];
			map<int, int> nei_dest = nei->dest_table;
			for(auto k = nei_dest.begin(); k != nei_dest.end(); k++){
				int dvdist = k->second;
				int desti = k->first;
					if (nei->path[desti] == i->first) continue;
					if (dvdist == -999 && current->path[desti]==nei_index) {i->second->dest_table[desti] = -999;}
					if (current->path[desti] == nei_index && dvdist+dist > curr_dest[desti]) i->second->dest_table[desti] = dvdist+dist;
					else if(((dvdist + dist < curr_dest[desti] || curr_dest[desti] == -999 ) && dvdist != -999)){
						
						i->second->dest_table[desti] = dvdist+dist;
						i->second->path[desti] = nei_index;
					}

					else if (dvdist + dist == curr_dest[desti] && nei_index < current->path[desti] && dvdist != -999)
					 	i->second->path[desti] = nei_index;

					//if (dvdist == -999 && curr_dest[desti])
 
			}
		}

	}
	
	return temp;
}


void sendmessages(map<int, r_node*>& table, char* file){
	msg = fopen(file, "r");

	char s[256];
	memset(s, '\0', 256);
	while (fgets(s, 256, msg)){
		char* message = NULL;
		int count = 0;
		int src, dst;
		for(int i = 0; i < strlen(s); i++){
			if (s[i] == ' ') count++;
			if (count == 2){
				s[i] = '\0';
				message = &s[i+1];
			}
		}
		sscanf(s, "%d %d", &src, &dst);
		if (table[src]->dest_table[dst] < 0){
			fprintf(fpOut, "from %d to %d cost infinite hops unreachable message %s", src, dst, message);
			fclose(msg);
			return;
		}
		fprintf(fpOut, "from %d to %d cost %d hops ", src, dst, table[src]->dest_table[dst]);

		int curr = src;
		while (curr != dst){
			fprintf(fpOut, "%d ", curr);
			curr = table[curr]->path[dst];
		}
		fprintf(fpOut, "message %s", message);
		memset(s, '\0', 256);
	}
	fclose(msg);
}

void writetable(map<int, r_node*>& routetables){
    for (auto it = routetables.begin(); it != routetables.end(); it++){
        r_node * singletable = it->second;
        for (auto itit = singletable->dest_table.begin(); itit != singletable->dest_table.end(); itit++){
        	if (itit->second == -999) continue;
            fprintf(fpOut, "%d %d %d\n", itit->first,it->second->path[itit->first], itit->second);
        }
    }
    // fclose(fpOut);
}

map<int, r_node*> erase_link(map<int, r_node*>& node_dict1, int end1, int end2, int dist1){

	vector<int> end1toend2;
	vector<int> end2toend1;

	for(auto i = node_dict1.begin(); i != node_dict1.end(); i++){
		if (node_dict1[end1]->path[i->first] == end2) {end1toend2.push_back(i->first); cout<<"a"<<endl;}
		if (node_dict1[end2]->path[i->first] == end1) {end2toend1.push_back(i->first); cout<<"b"<<endl;}
	}

	if(dist1 == -999){
		//cout<<"kkk"<<endl;
		node_dict1[end1]->dest_table[end2]=-999;
		node_dict1[end2]->dest_table[end1]=-999;
		node_dict1[end1]->neighbor[end2]=-999;
		node_dict1[end2]->neighbor[end1]=-999;
		//cout<<"kkk"<<endl;
		for(int i = 0; i < end1toend2.size(); i++){
			//cout<<"kkk1"<<endl;
			node_dict1[end1]->dest_table[end1toend2[i]]=-999;
			node_dict1[end1toend2[i]]->dest_table[end1]=-999;
		}
		cout<<"kkk2"<<endl;
		for(int i = 0; i < end2toend1.size(); i++){
			node_dict1[end2]->dest_table[end2toend1[i]]=-999;
			node_dict1[end2toend1[i]]->dest_table[end2]=-999;
		}
		cout<<"kkk3"<<endl;
		return node_dict1;
	}

	int diff = dist1-node_dict1[end1]->neighbor[end2];
	cout<<"123"<<endl;
	node_dict1[end1]->neighbor[end2]=dist1;
	node_dict1[end2]->neighbor[end1]=dist1;

	for(int i = 0; i < end1toend2.size(); i++){
		node_dict1[end1]->dest_table[end1toend2[i]]+=diff;
		node_dict1[end1toend2[i]]->dest_table[end1]+=diff;
	}
	for(int i = 0; i < end2toend1.size(); i++){
		node_dict1[end2]->dest_table[end2toend1[i]]+=diff;
		node_dict1[end2toend1[i]]->dest_table[end2]+=diff;
	}

	return node_dict1;

}

int main(int argc, char** argv) {
    //printf("Number of arguments: %d", argc);
    if (argc != 4) {
        printf("Usage: ./distvec topofile messagefile changesfile\n");
        return -1;
    }

 
    fpOut = fopen("output.txt", "w");

    map<int, r_node*> temp;

    int curr_n,nei_n,dist;


    ifstream myfile (argv[1]);
    if (myfile.is_open()){
		while(myfile>>curr_n>>nei_n>>dist){


			if(temp.find(curr_n )==temp.end()){
				r_node * topo_node = new r_node;
				topo_node->path[curr_n] = curr_n;
				temp[curr_n] = topo_node;
			}
			temp[curr_n]->neighbor[nei_n] = dist;
			temp[curr_n]->path[nei_n] = nei_n;
			temp[curr_n]->dest_table[nei_n] = dist;
			if(temp.find(nei_n) ==temp.end()){
				r_node * topo1_node = new r_node;
				topo1_node->path[nei_n] = nei_n;
				temp[nei_n] = topo1_node;
			}
			temp[nei_n]->neighbor[curr_n] = dist;
			temp[nei_n]->path[curr_n] = curr_n;
			temp[nei_n]->dest_table[curr_n] = dist;
		}
	}

	myfile.close();
	map<int, r_node*> node_dict;
	for (auto i = temp.begin(); i != temp.end(); i++){
		for (auto j = temp.begin(); j != temp.end(); j++){
			if (i == j){
				i->second->dest_table[i->first] = 0;
			}
			else if (i->second->dest_table.find(j->first)==i->second->dest_table.end()){
				i->second->dest_table[j->first] = -999;
			}
			else continue;
		}

	}

	for (int e = 0; e <8; e++){
		node_dict = cost_table(temp);
		temp = node_dict;
	}


	writetable(temp);
	cout<<"finish initial"<<endl;
	sendmessages(temp, argv[2]);

	ifstream myfile1 (argv[3]);
	//int flag = 1;
    if (myfile1.is_open()){
    	while(myfile1>>curr_n>>nei_n>>dist){
    		//map<int, r_node*>  node_dict;
    		if(node_dict.find(curr_n) != node_dict.end() && node_dict.find(nei_n) !=node_dict.end()){
    			node_dict=erase_link(node_dict,curr_n, nei_n, dist);
    			goto label;
    		}

    		if(node_dict.find(curr_n) == node_dict.end()){
    			r_node * topo_node = new r_node;
    			topo_node->path[curr_n] = curr_n;
    			node_dict[curr_n] = topo_node;
    		}
			node_dict[curr_n]->neighbor[nei_n] = dist;
			node_dict[curr_n]->path[nei_n] = nei_n;
			node_dict[curr_n]->dest_table[nei_n] = dist;
			if(node_dict.find(nei_n) ==node_dict.end()){
				r_node * topo1_node = new r_node;
				topo1_node->path[nei_n] = nei_n;
				node_dict[nei_n] = topo1_node;
			}
			node_dict[nei_n]->neighbor[curr_n] = dist;
			node_dict[nei_n]->path[curr_n] = curr_n;
			node_dict[nei_n]->dest_table[curr_n] = dist;

			for (auto i = node_dict.begin(); i != node_dict.end(); i++){
				for (auto j = node_dict.begin(); j != node_dict.end(); j++){
					if (i == j){
						i->second->dest_table[i->first] = 0;
					}
					else if (i->second->dest_table.find(j->first)==i->second->dest_table.end()){
						i->second->dest_table[j->first] = -999;
					}
					else continue;
				}

			}
			label:
			for (int e = 0; e <10; e++){
				temp = cost_table(node_dict);
				node_dict = temp;
			}
			//if(flag){flag = 0; continue;}
			writetable(temp);
			sendmessages(temp, argv[2]);
    	}

    }
    myfile.close();
   // fclose(change);
    fclose(fpOut);

    return 0;
}

