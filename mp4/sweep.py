from os import system
from tqdm import tqdm
import numpy as np

class input_pars:
    def __init__(self, N, L, R, M, T):
        self.N = N
        self.L = L
        self.R = R
        self.M = M
        self.T = T

    def to_file(self):
        f = open("input.txt", "w")
        f.write("N ")
        f.write(str(self.N))
        f.write("\n")
        f.write("L ")
        f.write(str(self.L))
        f.write("\n")
        f.write("R")
        for r in self.R:
            f.write(" ")
            f.write(str(r))
        f.write("\n")
        f.write("M ")
        f.write(str(self.M))
        f.write("\n")
        f.write("T ")
        f.write(str(self.T))
        # f.write("\n")
        f.close()

class output_data:
    def __init__(self):
        self.utilization = 0
        self.idle = 0
        self.collisions = 0
        self.var_transmissions = 0
        self.var_collisions = 0

    def from_file(self):
        f = open("output.txt")
        s = f.readline()
        num = s.split()[-1]
        self.utilization = num
        s = f.readline()
        num = s.split()[-1]
        self.idle = num
        s = f.readline()
        num = s.split()[-1]
        self.collisions = num
        s = f.readline()
        num = s.split()[-1]
        self.var_transmissions = num
        s = f.readline()
        num = s.split()[-1]
        self.var_collisions = num
        f.close()

    def tostring(self):
        s = ""
        s += "Channel utilization "
        s += str(self.utilization)
        s += "\n"
        s += "Channel idle fraction "
        s += str(self.idle)
        s += "\n"
        s += "Total number of collisions "
        s += str(self.collisions)
        s += "\n"
        s += "Variance in number of successful transmissions "
        s += str(self.var_transmissions)
        s += "\n"
        s += "Variance in number of collisions "
        s += str(self.var_collisions)
        s += "\n"
        return s

def main():
    data = []
    for it in tqdm(range(5, 501)):
        i = input_pars(it, 20, [8,16,32,64,128,256,512], 6, 50000)
        i.to_file()
        system("./csma input.txt")
        o = output_data()
        o.from_file()
        data.append(o.idle)

    import matplotlib.pyplot as plt
    plt.ylim(5,500)
    plt.plot(np.arange(5, 501, 1), np.array(data))
    plt.xlabel('number of hosts')
    plt.ylabel('channel idle fraction')
    plt.title('channel idle VS N')
    # plt.yticks(np.arange(5,501,50))
    plt.show()

if __name__ == "__main__":
    main()



