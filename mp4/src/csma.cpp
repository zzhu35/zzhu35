#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <fstream>
#include <map>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

typedef struct{
    int id;
    int randlevel;
    int backoff;
    unsigned long collision;
    unsigned long success;
}host_t;

typedef struct{
    FILE* in;
    FILE* out;
    int num_hosts;
    int pkt_size;
    int max_retry;
    unsigned long timeout;
    unsigned long clk;
    int lock;
    unsigned long useful_epochs;
    unsigned long useless_epochs;
    unsigned long collisions;
    map<int ,long> randrange;
    int num_randrange;
    host_t* hosts;

}game_state_t;

static game_state_t state;

void game_setup(){
    state.lock = 0;
    state.clk = 0;
    state.num_randrange = 0;
    state.useful_epochs = 0;
    state.collisions = 0;
    char s[256];
    memset(s, '\0', 256);
    while (fgets(s, 256, state.in)){
        char* str = s;
        while(*str == ' ') str++;
        long val;
        switch(*str){
            case 'N':
                sscanf(str+1, "%d", &state.num_hosts);
                break;
            case 'L':
                sscanf(str+1, "%d", &state.pkt_size);
                break;
            case 'R':
                val = strtol(str+1, &str, 10);
                while (val){
                    state.randrange[state.num_randrange++] = val;
                    val = strtol(str, &str, 10);
                }
                break;
            case 'M':
                sscanf(str+1, "%d", &state.max_retry);
                break;
            case 'T':
                sscanf(str+1, "%lu", &state.timeout);
                break;
            default:
                break;
        }
        memset(s, '\0', 256);
    }
    state.hosts = (host_t*)malloc(state.num_hosts*sizeof(host_t));
    for (int i = 0; i < state.num_hosts; i++){
        host_t& host = state.hosts[i];
        host.id = i;
        host.randlevel = 0;
        host.backoff = rand() % state.randrange[host.randlevel];
        host.collision = 0;
        host.success = 0;
    }
    // printf("[INFO] Steup complete.\n\tFound %d hosts\n\tpacket size %d\n\tmax_retry %d\n\tsimulation time %lu\n\trange levels: %d levels\n\t", state.num_hosts, state.pkt_size, state.max_retry, state.timeout, state.num_randrange);
    // for (int i = 0; i < state.num_randrange; i++){
    //     printf("%ld ", state.randrange[i]);
    // }
    // printf("\n");
}

int game_loop(){
    // printf("[INFO] Iteration %lu\n", state.clk);
    if (state.lock){
        state.lock--;
        state.useful_epochs++;
    }
    else{
        vector<int> candidates;
        for (int i = 0; i < state.num_hosts; i++){
            if (state.hosts[i].backoff == 0){
                candidates.push_back(i);
            }
        }
        if (candidates.size() == 1){
            host_t& host = state.hosts[candidates[0]];
            host.backoff = rand() % state.randrange[host.randlevel];
            host.success++;
            state.lock = state.pkt_size;
        }
        else if (candidates.size() > 1){
            state.collisions++;
            for (auto it = candidates.begin(); it != candidates.end(); it++){
                host_t& host = state.hosts[*it];
                host.randlevel++;
                host.backoff = rand() % state.randrange[host.randlevel];
                host.collision++;
                if (host.collision >= state.max_retry){
                    host.collision = 0;
                    host.randlevel = 0;
                    host.backoff = rand() % state.randrange[host.randlevel];
                }
            }
        }
        else{
            for (int i = 0; i < state.num_hosts; i++){
                state.hosts[i].backoff--;
            }
            state.useless_epochs++;
        }
    }
    return (state.clk++ != state.timeout);
}

void game_close(){
    // printf("Channel utilization %.2lf\n", (double)state.useful_epochs/(double)state.timeout*(double)100);
    fprintf(state.out, "Channel utilization %.2lf\n", (double)state.useful_epochs/(double)state.timeout*(double)100);
    // printf("Channel idle fraction %.2lf\n", (double)state.useless_epochs/(double)state.timeout*(double)100);
    fprintf(state.out, "Channel idle fraction %.2lf\n", (double)state.useless_epochs/(double)state.timeout*(double)100);
    // printf("Total number of collisions %lu\n", state.collisions);
    fprintf(state.out, "Total number of collisions %lu\n", state.collisions);
    double avgsuccess = 0.0f;
    double avgcollission = 0.0f;
    double varsuccess = 0.0f;
    double varcollision = 0.0f;
    for (int i = 0; i < state.num_hosts; i++){
        avgsuccess += (double)state.hosts[i].success/(double)state.num_hosts;
        avgcollission += (double)state.hosts[i].collision/(double)state.num_hosts;
    }
    for (int i = 0; i < state.num_hosts; i++){
        varsuccess += pow((double)state.hosts[i].success-avgsuccess, 2);
        varcollision += pow((double)state.hosts[i].collision-avgcollission, 2);
    }

    varcollision /= (double)state.num_hosts;
    varsuccess /= (double)state.num_hosts;
    // printf("Variance in number of successful transmissions (across all nodes) %.2lf\n", varsuccess);
    fprintf(state.out, "Variance in number of successful transmissions (across all nodes) %.2lf\n", varsuccess);
    // printf("Variance in number of collisions (across all nodes) %.2lf\n", varcollision);
    fprintf(state.out, "Variance in number of collisions (across all nodes) %.2lf\n", varcollision);
    free(state.hosts);

};


int main(int argc, char** argv) {
    if (argc != 2) {
        printf("Usage: ./csma input.txt\n");
        return -1;
    }
    state.in = fopen(argv[1], "r");
    state.out = fopen("output.txt", "w");

    game_setup();
    while(game_loop());
    game_close();



    fclose(state.in);
    fclose(state.out);

    return 0;
}

