/* 
 * File:   receiver_main.c
 * Author: 
 *
 * Created on
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <pthread.h>

#include "packet.h"
#include "debug.h"



struct sockaddr_in si_me, si_other;
int s, slen;
volatile int not_fin = 1;

int max_written = 0;

void diep(char *s) {
    perror(s);
    exit(1);
}

void print_list(pkt_node_t* head){
    if (!head) return;
    while(head){
        printf("%d", head->data.header.seq);
        head = head->next;
        if (head) printf(" ");
    }
    printf("\n");
}

void store(pkt_node_t* head, pkt_node_t* new){
    // we have made sure that head is not NULL and head's seq is smaller than new, but just check if anyways
    if (!head){
        rprintf("ll screwed up...\n");
        return;
    }
    if (!new) return;
    // find a place to put this new packet in order
    pkt_node_t* curr = head, *next = curr->next;
    while (next){
        if (next->data.header.seq > new->data.header.seq) break;
        curr = curr->next;
        next = next->next;
    }
    if (curr->data.header.seq == new->data.header.seq){
        free(new);
        return;
    }
    curr->next = new;
    new->next = next;
}


void list_to_file(pkt_node_t** headptr, FILE* f){
    if (!headptr) return;
    pkt_node_t* head = *headptr;
    if (!head) return;
    if (!f) return;
    int i = max_written + 1;
    while (head){
        if (i == head->data.header.seq){ // if packets are in order
            // check if terminator packet
            if (head->data.header.length > MAXPAYLOAD){
                not_fin = 0;
                *headptr = head;
                return;
            }
            int bytes_written = fwrite((void*)(head->data.payload), 1, head->data.header.length, f);
            gprintf("Written packet %d to file\n", i);
            max_written = i;
            if (bytes_written != head->data.header.length){
               rprintf("Very wierd...\n");
            }
        }
        else{
            break;
        }
        i++;
        pkt_node_t* tofree = head;
        head = head->next;
        free(tofree);
    }
    *headptr = head;
}

void ack(unsigned int seq){
    pkt_h_t ack;
    ack.length = 0;
    ack.seq = seq;
    int bytes_ack = sendto(s, (void*)&ack, sizeof(pkt_h_t), 0, (const struct sockaddr*)&si_other, slen);
    if (bytes_ack != sizeof(pkt_h_t)){
        rprintf("ACK wierdness detected...\n");
    }
    bprintf("Sent ACK %u\n", seq);
}

void reliablyReceive(unsigned short int myUDPport, char* destinationFile) {
    
    slen = sizeof (si_other);


    if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
        diep("socket");

    memset((char *) &si_me, 0, sizeof (si_me));
    si_me.sin_family = AF_INET;
    si_me.sin_port = htons(myUDPport);
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);
    printf("Now binding\n");
    if (bind(s, (struct sockaddr*) &si_me, sizeof (si_me)) == -1)
        diep("bind");


	/* Now receive data and send acknowledgements */
    FILE* f = fopen(destinationFile,"wb");
    unsigned int expected = 1;
    pkt_node_t* head = NULL;

    while(not_fin){
        list_to_file(&head, f);
        pkt_node_t* newpkt = NULL;
        while (!newpkt){
            newpkt = (pkt_node_t*)malloc(sizeof(pkt_node_t));
        }
        newpkt->next = NULL;
        int bytes_recv = recvfrom(s, (void*)&(newpkt->data), sizeof(pkt_t), 0, (struct sockaddr*)&si_other, (socklen_t*)&slen);
        if (bytes_recv != sizeof(pkt_t)){
            rprintf("Expecting %ld bytes but got %d\n", sizeof(pkt_t), bytes_recv);
        }
        gprintf("Received packet %u\n", newpkt->data.header.seq);
        ack(max_written);
        // if received old packets
        if (newpkt->data.header.seq < expected){
            ack(max_written);
            continue;
        }
        // packet in window, insert packet in bufffer
        if (!head){
            head = newpkt;
        }
        else if (head->data.header.seq > newpkt->data.header.seq){ //insert at beginning of buffer
            newpkt->next = head;
            head = newpkt;
        }
        else{
            store(head, newpkt); // must be after first element in buffer, no need to pass double pointer
        }
        print_list(head);
        list_to_file(&head, f);
        print_list(head);

        expected = max_written + 1;
        if (!not_fin) ack(max_written + 1);
        else ack(max_written);
    }

    fclose(f);
    close(s);
	gprintf("\n******\n%s received.\n", destinationFile);
    return;
}

/*
 * 
 */
int main(int argc, char** argv) {

    unsigned short int udpPort;

    if (argc != 3) {
        fprintf(stderr, "usage: %s UDP_port filename_to_write\n\n", argv[0]);
        exit(1);
    }

    udpPort = (unsigned short int) atoi(argv[1]);

    reliablyReceive(udpPort, argv[2]);
}

