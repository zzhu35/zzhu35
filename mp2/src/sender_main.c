/* 
 * File:   sender_main.c
 * Author: 
 *
 * Created on 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/stat.h>
#include <signal.h>
#include <string.h>
#include <sys/time.h>

#include "transfer_control.h"
#include "packet.h"


struct sockaddr_in si_other;
int s, slen;
volatile tc_t control;
volatile int fin_to = 0;
int fin = 0;

void diep(char *s) {
    perror(s);
    exit(1);
}

void print_list(pkt_node_t* head){
    if (!head) return;
    while(head){
        printf("%d", head->data.header.seq);
        head = head->next;
        if (head) printf(" ");
    }
    printf("\n");
}

// int check_timeout_pkt(pkt_node_t* curr){
//     if (!curr) return 0;
//     int ttl = TIMEOUT - ((clock() - curr->timesent) * 1000 / CLOCKS_PER_SEC); // in msec
//     if (ttl < 0){
//         //control.to = 1;
//         if (curr->data.header.seq == fin) fin_to++;
//         // re-TX timeout packet
//         bprintf("Re-sent t/o packet %u\n", curr->data.header.seq);
//         sendto(s, (void*)&(curr->data), sizeof(pkt_t), 0, (const struct sockaddr*)&si_other, slen);
//         curr->timesent = clock();
//         return 1;
//     }
//     return 0;
// }

// void* check_timeout(){
//     while(!control.fin){
//         control_update(&control);
//         clock_t prev = clock();
//         while(TIMEOUT - ((clock() - prev) * 1000 / CLOCKS_PER_SEC) > 0);
//         /********************
//          * critical section *
//          ********************/
//         pthread_spin_lock(&(control.lock));
//         pkt_node_t* curr = control.head;
//         while(curr){
//             if (check_timeout_pkt(curr)){
//                 if (curr->next){
//                     curr->next->timesent = clock();
//                 }
//                 break;
//             }
//             curr = curr->next;
//         }
//         pthread_spin_unlock(&(control.lock));
//         /********************
//          * critical section *
//          ********************/
//     }
//     return NULL;
// }

void* check_timeout(){
    int prev = 0;
    while(!control.fin){
        control_update(&control);
        clock_t ct = clock();
        while(TIMEOUT - ((clock() - ct) * 1000 / CLOCKS_PER_SEC) > 0);
        /********************
         * critical section *
         ********************/
        pthread_spin_lock(&(control.lock));
        if (control.head){
            if (prev == control.head->data.header.seq){
                bprintf("Re-sent t/o packet %u\n", control.head->data.header.seq);
                // control.to = 1;
                sendto(s, (void*)&(control.head->data), sizeof(pkt_t), 0, (const struct sockaddr*)&si_other, slen);
            }
            prev = control.head->data.header.seq;
        }
        pthread_spin_unlock(&(control.lock));
        /********************
         * critical section *
         ********************/
    }
    return NULL;
}

void* receive_acks(){
    pkt_h_t newheader;
    while(1){
        control_update(&control);
        /*int recvd = */recvfrom(s, (void*)&newheader, sizeof(pkt_h_t), 0, (struct sockaddr*)&si_other, (socklen_t*)&slen);
        gprintf("Got ACK %d\n", newheader.seq);
        // check if this is last ack
        if ((control.fin_send_new && newheader.seq == control.max_sent) || (fin_to > FIN_TO_TOLERANCE)){
            control.fin = 1; // tell main sender to exit as well, we are done
            close(s);
            return NULL;// well we don't care about memory leak and leave the housekeeping job with the OS
        }
        // ignore small acks
        control.newack = 0;
        if (newheader.seq < control.max_ack) continue;
        // add dupack
        if (newheader.seq == control.max_ack){
            control.dupack++;
            if (control.mode == FAST_RCVRY) control.reward++; // only reward dupack if in fast recovery mode
            continue;
        }
        // new ack
        control.newack = 1;
        control.reward += newheader.seq - control.max_ack;
        control.max_ack = newheader.seq;


        // starting from head, remove everything less than current ack
        /********************
         * critical section *
         ********************/
        pthread_spin_lock(&(control.lock));
        while (control.head){
            if (control.head->data.header.seq <= newheader.seq){
                pkt_node_t* tofree = control.head;
                control.head = control.head->next;
                free(tofree);
            }
            else{
                break;
            }
        }
        if (!control.head){
            pthread_spin_unlock(&(control.lock));
            continue; // already removed everything, then go to receive
        }
        pkt_node_t* curr = control.head->next, *prev = control.head;
        while(curr){
            if (curr->data.header.seq <= newheader.seq){
                pkt_node_t* tofree = curr;
                prev->next = curr->next;
                curr = curr->next;
                free(tofree);
            }
            else{
                prev = curr;
                curr = curr->next;
            }
        }
        pthread_spin_unlock(&(control.lock));
        /********************
         * critical section *
         ********************/
    }
}

void reliablyTransfer(char* hostname, unsigned short int hostUDPport, char* filename, unsigned long long int bytesToTransfer) {
    //Open the file
    clock_t start_timer = clock();
    FILE *fp;
    fp = fopen(filename, "rb");
    if (fp == NULL) {
        printf("Could not open file to send.");
        exit(1);
    }

    slen = sizeof (si_other);

    if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
        diep("socket");

    memset((char *) &si_other, 0, sizeof (si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(hostUDPport);
    if (inet_aton(hostname, &si_other.sin_addr) == 0) {
        fprintf(stderr, "inet_aton() failed\n");
        exit(1);
    }

	/* Send data and receive acknowledgements on s*/
    
    control_init(&control);
    pthread_t listener, checker;
    pthread_create(&listener, NULL, receive_acks, NULL);
    pthread_create(&checker, NULL, check_timeout, NULL);
    

    // main sender thread
    while (!control.fin){
        // send possible packets
        if (!control.fin_send_new){
            int seq;
            // haha, we just don't support sequence wrapping
            for (seq = control.max_sent + 1; seq <= (control.max_ack + control.cw) && !control.fin_send_new; seq++){ // send these packets
                pkt_node_t* newpkt = NULL;
                while (!newpkt){
                    newpkt = (pkt_node_t*)malloc(sizeof(pkt_node_t));
                }
                newpkt->next = NULL;
                newpkt->data.header.seq = seq;
                control.max_sent++;
                if (bytesToTransfer == 0){
                    // send a packet with crazy length which exceeds MAXPAYLOAD and let receiver detect
                    newpkt->data.header.length = MAXPAYLOAD + 1;
                    control.fin_send_new = 1;
                    fin = seq;
                }
                else if (bytesToTransfer > MAXPAYLOAD){
                    newpkt->data.header.length = fread((void*)(newpkt->data.payload), 1, MAXPAYLOAD, fp);
                    bytesToTransfer -= newpkt->data.header.length;
                    if (newpkt->data.header.length == 0){
                        bytesToTransfer = 0;
                    }
                }
                else{
                    newpkt->data.header.length = fread((void*)(newpkt->data.payload), 1, bytesToTransfer, fp);
                    bytesToTransfer = 0;
                }
                
                // push to linked list
                /********************
                 * critical section *
                 ********************/
                pthread_spin_lock(&(control.lock));
                // insert at tail
                if (!control.head){
                    control.head = newpkt;
                }
                else{
                    pkt_node_t* curr = control.head;
                    while(curr->next){
                        curr = curr->next;
                    }
                    curr->next = newpkt;
                }
                pthread_spin_unlock(&(control.lock));
                /********************
                 * critical section *
                 ********************/
                // send content
                newpkt->timesent = clock();
                bprintf("Sent packet %u\n", newpkt->data.header.seq);
                int bytes_sent = sendto(s, (void*)&(newpkt->data), sizeof(pkt_t), 0, (const struct sockaddr*)&si_other, sizeof(struct sockaddr_in));
                if (bytes_sent != sizeof(pkt_t)) rprintf("Error detected at packet %u: Only sent %d bytes as opposed to a whole packet\n", newpkt->data.header.seq, bytes_sent);
            }
        }
    }

    pthread_join(listener, NULL);
    pthread_join(checker, NULL);

    gprintf("\n******\nClosing the socket.\nUsed %lds\n",(clock() - start_timer) / CLOCKS_PER_SEC);
    fclose(fp);
    close(s);
    return;

}

/*
 * 
 */
int main(int argc, char** argv) {

    unsigned short int udpPort;
    unsigned long long int numBytes;

    if (argc != 5) {
        fprintf(stderr, "usage: %s receiver_hostname receiver_port filename_to_xfer bytes_to_xfer\n\n", argv[0]);
        exit(1);
    }
    udpPort = (unsigned short int) atoi(argv[2]);
    numBytes = atoll(argv[4]);
    reliablyTransfer(argv[1], udpPort, argv[3], numBytes);
    return (EXIT_SUCCESS);
}


