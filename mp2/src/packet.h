#ifndef _PACKET_H
#define _PACKET_H

#include <time.h>

// #define MAXPAYLOAD 32768 // 32KB
#define MAXPAYLOAD 8000


typedef struct packet_header{
    unsigned int seq;
    unsigned int length;
}pkt_h_t;

typedef struct packet{
    pkt_h_t header;
    char payload[MAXPAYLOAD];
}pkt_t;

typedef struct packet_node{
    struct packet_node* next;
    pkt_t data;
    clock_t timesent;
}pkt_node_t;


#endif // _PACKET_H