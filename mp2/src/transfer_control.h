#ifndef _TRANSFER_CONTROL_H
#define _TRANSFER_CONTROL_H

#include <pthread.h>

#include "debug.h"
#include "packet.h"

// TUNE ME!
#define SLOW_START_THRESHOLD 32
#define TIMEOUT 55
#define DUPACK_THRESHOLD 10
#define FIN_TO_TOLERANCE 5

#define SLOW_START 0
#define CONJ_AVOID 1
#define FAST_RCVRY 2

typedef struct transfer_control{
    pthread_spinlock_t lock;
    volatile unsigned int sst;
    volatile unsigned int cw;
    volatile unsigned int reward;
    volatile unsigned int mode;
    volatile unsigned int dupack;
    volatile unsigned int newack;
    volatile unsigned int to;
    volatile unsigned int max_sent;
    volatile unsigned int max_ack;
    volatile unsigned int fin_send_new;
    volatile unsigned int fin;
    pkt_node_t* head;
}tc_t;

void control_init(volatile tc_t* state);
void redeem_rewards(volatile tc_t* state);
void control_update(volatile tc_t* state);

#endif // _TRANSFER_CONTROL_H