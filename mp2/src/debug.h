/*
    Utils for colored outputs
*/


#ifndef _DEBUG_H
#define _DEBUG_H

#include <stdio.h>

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

#define rprintf(...) do{\
    printf(KRED);\
    printf(__VA_ARGS__);\
    printf(KWHT);\
}while(0)

#define gprintf(...) do{\
    printf(KGRN);\
    printf(__VA_ARGS__);\
    printf(KWHT);\
}while(0)

#define bprintf(...) do{\
    printf(KBLU);\
    printf(__VA_ARGS__);\
    printf(KWHT);\
}while(0)

#define yprintf(...) do{\
    printf(KYEL);\
    printf(__VA_ARGS__);\
    printf(KWHT);\
}while(0)

#endif // _DEBUG_H