#include "transfer_control.h"


void control_init(volatile tc_t* state){
    if (!state){
        rprintf("State struct pointer is NULL\n");
        return;
    }
    state->sst = SLOW_START_THRESHOLD;
    state->cw = 1;
    state->reward = 0;
    state->mode = SLOW_START;
    state->dupack = 0;
    state->newack = 0;
    state->to = 0;
    state->max_sent = 0;
    state->max_ack = 0;
    state->head = NULL;
    state->fin = 0;
    state->fin_send_new = 0;
    pthread_spin_init(&(state->lock), PTHREAD_PROCESS_SHARED);
}

void redeem_rewards(volatile tc_t* state){
    while (1){
        if (state->mode == SLOW_START || state->mode == FAST_RCVRY){
            if (state->reward == 0) break;
            state->cw++;
            state->reward--;
        }
        else if (state->mode == CONJ_AVOID){
            if (state->reward < state->cw) break;
            state->reward -= state->cw;
            state->cw++;
        }
        else{
            break;
        }
    }
    if (state->mode == SLOW_START && state->cw == state->sst){
        state->mode = CONJ_AVOID;
    }
}

void control_update(volatile tc_t* state){
    if (!state){
        rprintf("State struct pointer is NULL\n");
        return;
    }
    redeem_rewards(state);

    //resolve threashold
    if (state->mode == SLOW_START && state->cw == state->sst){
        yprintf("Going to Conjestion Avoidance\n");
        state->mode = CONJ_AVOID;
    }

    //resolve dupack == 3
    else if (state->dupack == DUPACK_THRESHOLD){
        yprintf("Going to Fast Revocery\n");
        state->mode = FAST_RCVRY;
        state->cw >>= 1;
        state->sst = state->cw;
        state->reward += DUPACK_THRESHOLD;
    }

    //resolve timeout
    else if (state->to){
        yprintf("Going to Slow Start\n");

        state->mode = SLOW_START;
        state->sst = state->cw >> 1;
        state->cw = 1;
        state->dupack = 0;
        state->to = 0;
    }

    //resolve new ack recovery
    else if (state->mode == FAST_RCVRY && state->newack){
        state->cw = state->sst;
        state->dupack = 0;
    }

    //nothing
    else{

    }

    redeem_rewards(state);
    if (state->cw < 1) state->cw = 1;
    if (state->sst < 1) state->sst = 1;
}